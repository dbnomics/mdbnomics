function runalltests()

% Copyright (C) 2020 Dynare Team
%
% This code is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare dseries submodule is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.
mdbnomics_src_root = []; % Will be updated by calling initialize_dseries_class().

opath = path();

system('rm -f failed');
system('rm -f pass');

% Check that the m-unit-tests module is available.
try
    initialize_unit_tests_toolbox;
catch
    error('Missing dependency: m-unit-tests module is not available.')
end

% Get path to the current script
unit_tests_root = strrep(which('runalltests'),'runalltests.m','');

% Initialize the mdbnomics module
try
    initialize_mdbnomics();
catch
    addpath([unit_tests_root '../src']);
    initialize_mdbnomics();
end

warning off

if isoctave()
    if ~user_has_octave_forge_package('io')
        error('Missing dependency: io package is not available.')
    end
    more off;
end

r = run_unitary_tests_in_directory(mdbnomics_src_root(1:end-1));

delete('*.log');

if any(~[r{:,3}])
    system('touch failed');
else
    system('touch pass');
end

warning on
path(opath);

display_report(r);
