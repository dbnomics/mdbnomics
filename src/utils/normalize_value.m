function series = normalize_value(series)
% function normalize_value(series)
% Modifies series. Keeps original value and converts "NA" to NaN.
% Returns modified series.
%
% INPUTS
%   series                  [struct]           series struct returned after period normalization
%
% OUTPUTS
%   series
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

if iscell(series.value)
    series.original_value = cellfun(@num2str,series.value,'un',0);
    series.value(strcmp(series.value, 'NA')) = {NaN};
else
    series.original_value = num2cell(series.value);
    series.value = num2cell(series.value);
end
end
